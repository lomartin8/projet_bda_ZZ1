<?php include "includes/headers.php"; ?>
<div class="container">
	<?php 
	
		// $_GET['id'] pour recuperer une variable passee en parametre
		// isset($var) && is_numeric($var)
		
	
		if (isset($_GET['id']) && is_numeric($_GET['id'])) {
		
			$resultats = $bdd->query("SELECT * FROM news WHERE id='".$_GET['id']."'");
		} 
		
		else {
				
			$resultats = $bdd->query("SELECT * FROM news ORDER BY date DESC LIMIT 10");
			
		}
		foreach ($resultats as $courant){
	
			echo ' <div class="row">
						<div class="col-lg-12">
							<div class="box">
								<div class="col-lg-12">
								   	<hr>
								   	<h2 class="intro-text text-center">
								   		'. $courant['titre'] .'
								  	</h2>
									<hr>
								</div>
								<div class="col-md-6">
									<img class="img-responsive img-border-left imgtransparente" src="'. $courant['img'] .'" alt="">
								</div>
								<div class="col-md-6">
									<p>'.nl2br($courant['contenu']).'</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
			
				   </div> ';
		}
	
	?>
</div>
<?php include "includes/footer.php"; ?>
