 <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Créé par BOONE Valentine et MARTIN Louis en 2017</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

	<script defer src="./lightbox/js/lightbox.js"></script>
	<link defer rel="stylesheet" href="./lightbox/css/lightbox.css" type="text/css" />
    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
	$('.ai').click(function(){
		$('.ai').removeClass('open');
		$(this).addClass('open');
	});
    </script>

</body>

</html>
