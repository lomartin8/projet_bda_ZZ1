<?php

$bdd = new PDO('mysql:host=localhost;dbname=bdd_bda', 'louis', 'louismartin');

/* Code minimum accès BDD
		$resultats = $bdd->query("SELECT img, titre, contenu FROM news");
		foreach ($resultats as $courant){


			print "<img src='" . $courant['img'] . "' />";
		}
*/

/*******************PAGE D'ACCUEIL : AFFICHER UNE MUSIQUE AU HASARD*************************/

/*chercher une musique au hasard dans la base de données*/

function music_random($bdd){

	$musique = $bdd->query("SELECT musiques.lien, musiques.nom, sousgenre.sousgenre, artiste.nom as nom_artiste, artiste.pays 
							FROM musiques, sousgenre, artiste 
							WHERE musiques.id_genre=sousgenre.id AND musiques.id_artiste=artiste.id 
							AND musiques.lien LIKE '%youtube%'
							ORDER BY RAND() LIMIT 1");
	$info=$musique->fetch();
	echo videoType($info); /*fetch récupère la 1ère ligne contenue dans $musique*/
	
	
	return $info;
}

/*sélectionner l'url de cette musique*/



/*code récupéré et modifié sur https://stackoverflow.com/questions/6618967/php-how-to-check-whether-the-url-is-youtubes-or-vimeos pour identifier l'origine de la vidéo*/

function videoType($musique) {
    
    if (strpos($musique["lien"], 'youtube') > 0) /*cas lien type youtube : recuperation du lien pour afficher la video yt correspondante*/
    {
    
    	$id=str_replace("https://www.youtube.com/watch?v=", "", $musique["lien"]);
    	$id=str_replace("https://youtu.be/","",$id);
    	return '<iframe style="width:100%" height="315" src="https://www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>';
       
    } 
   
    elseif (strpos($musique["lien"], 'bandcamp') > 0) /*cas lien type bandcamp, on renvoie directement sur le site*/
    {
        return '<a target="_blank" href="'.$musique["lien"].'">'.$musique["nom"].'</a> ';
    } 
    
    else 
    {
        return 'unknown';
    }
}





